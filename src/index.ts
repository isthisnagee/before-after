(function() {
  // ************
  // General Info
  // ************
  //
  // Everything in this file lives in an Immediately Invoked Function
  // Expression. We do this to avoid polluting the global namespace.
  //
  // ==============
  // File Structure
  // ==============
  //
  // Each section contains a title and a paragraph defining its use.
  // All functions are defines at the bottom of the file, taking advantage
  // of function hoisting in JavaScript. Functions should go in their appropriate
  // a section gets longer than 100 lines, feel free to move it to its
  // own file.
  //
  // Each function should have its input and output types defined.

  // *****
  // TYPES
  // *****
  //
  // This section will contain all shared types.

  type FormValue = {
    general: {
      fileName: string;
      canDraw: boolean;
    };
    before: {
      file: File | null;
      text: string | null;
    };
    after: {
      file: File | null;
      text: string | null;
    };
  };

  type NonFormState = {
    drawing: {
      isDrawing: boolean;
      prevX: number;
      prevY: number;
    };
  };

  enum Side {
    before = 'before',
    after = 'after',
  }

  // *******
  // GLOBALS
  // *******
  //
  // This section defines all HTML elements, magic numbers, and string constants.

  const INITIAL = {
    fileName: 'before-v-after',
    before: 'before',
    after: 'after',
    canDraw: false,
  };

  const STATE: FormValue & NonFormState = {
    general: {
      fileName: INITIAL.fileName,
      canDraw: INITIAL.canDraw,
    },
    before: {
      file: null,
      text: INITIAL.before,
    },
    after: {
      file: null,
      text: INITIAL.after,
    },
    drawing: {
      isDrawing: false,
      prevX: 0,
      prevY: 0,
    },
  };

  const $ = {
    general: {
      fileName: document.getElementById(
        'general--file-name',
      ) as HTMLInputElement,
      enableDrawing: document.getElementById(
        'general--enable-drawing',
      ) as HTMLInputElement,
      clearDrawing: document.getElementById(
        'general--clear-drawing',
      ) as HTMLButtonElement,
    },
    before: {
      file: document.getElementById('before--file') as HTMLInputElement,
      text: document.getElementById('before--text') as HTMLInputElement,
    },
    after: {
      file: document.getElementById('after--file') as HTMLInputElement,
      text: document.getElementById('after--text') as HTMLInputElement,
    },
    content: {
      canvasWrapper: document.getElementById('scale') as HTMLDivElement,
      canvas: document.getElementById('canvas') as HTMLCanvasElement,
      drawingCanvas: document.getElementById(
        'drawing-canvas',
      ) as HTMLCanvasElement,
    },
    submit: document.getElementById('submit') as HTMLButtonElement,
    output: document.getElementById('output-img') as HTMLLinkElement,
  };

  // ****
  // MAIN
  // ****
  //
  // This section is _only_ for the main function.

  main();

  function main(): void {
    // This is experimental browser technology. We should find a solution that
    // will work in Safari.
    const resizeObserver = new window.ResizeObserver(handleResize);
    resizeObserver.observe($.content.canvasWrapper);

    $.general.fileName.value = STATE.general.fileName;
    $.general.fileName.onchange = validateForm(handleFileNameChange);
    $.general.enableDrawing.checked = STATE.general.canDraw;
    $.general.enableDrawing.onclick = handleEnableDrawing;
    $.general.clearDrawing.onclick = handleClearDrawing;

    $.before.file.onchange = validateForm(handleAddFile(Side.before));
    $.before.file.value = '';
    $.before.text.oninput = handleTextNameChange(Side.before);
    $.before.text.value = STATE.before.text || '';

    $.after.file.onchange = validateForm(handleAddFile(Side.after));
    $.after.file.value = '';
    $.after.text.oninput = handleTextNameChange(Side.after);
    $.after.text.value = STATE.after.text || '';

    $.submit.onclick = handleSubmit;
    $.submit.disabled = true;

    $.content.drawingCanvas.onmousedown = handleBeginPenDrawing;
    $.content.drawingCanvas.onmouseup = handleEndPenDrawing;
    $.content.drawingCanvas.onmouseout = handleEndPenDrawing;
    $.content.drawingCanvas.onmousemove = handleMaybeDrawWithPen;

    draw();
  }

  // ********
  // HANDLERS
  // ********
  //
  // This section contains code that handles HTML events, like `onclick` and
  // `onchange`,

  function handleResize(entries: ResizeObserverEntry[]) {
    const [canvasWrapper] = entries;
    const {width, height} = canvasWrapper.contentRect;
    setTimeout(() => {
      $.content.canvas.width = width;
      $.content.canvas.height = height;
      $.content.drawingCanvas.width = width;
      $.content.drawingCanvas.height = height;

      draw();
    });
  }

  function handleBeginPenDrawing(e: MouseEvent): void {
    if (!STATE.general.canDraw) return;
    STATE.drawing.isDrawing = true;
    STATE.drawing.prevX = e.offsetX;
    STATE.drawing.prevY = e.offsetY;
  }
  function handleEndPenDrawing(): void {
    STATE.drawing.isDrawing = false;
  }

  function handleMaybeDrawWithPen(e: MouseEvent): void {
    if (!STATE.general.canDraw) {
      handleEndPenDrawing();
    }
    if (!STATE.drawing.isDrawing) return;
    const {prevX, prevY} = STATE.drawing;
    const ctx = getContext($.content.drawingCanvas);

    ctx.beginPath();
    ctx.moveTo(prevX, prevY);
    ctx.lineTo(e.offsetX, e.offsetY);
    ctx.stroke();

    STATE.drawing.prevX = e.offsetX;
    STATE.drawing.prevY = e.offsetY;
  }

  function handleFileNameChange(e: Event): void {
    const currentValue = (e.currentTarget as HTMLInputElement).value;
    STATE.general.fileName = currentValue;
  }

  function handleEnableDrawing(e: MouseEvent): void {
    STATE.general.canDraw = (e.currentTarget as HTMLInputElement).checked;
  }

  function handleClearDrawing(): void {
    const ctx = getContext($.content.drawingCanvas);
    const {width, height} = ctx.canvas;
    ctx.clearRect(0, 0, width, height);
  }

  function handleTextNameChange(side: Side): (e: Event) => void {
    return (e: Event) => {
      const currentValue = (e.target as HTMLInputElement).value;
      setTimeout(() => {
        STATE[side].text = currentValue;
        drawSide(side);
      }, 300);
    };
  }

  function handleSubmit(): void {
    const {canvas, drawingCanvas} = $.content;
    const outputCanvas = document.createElement('canvas');
    outputCanvas.width = canvas.width;
    outputCanvas.height = canvas.height;

    const ctx = getContext(outputCanvas);

    ctx.fillStyle = 'white'
    ctx.fillRect(0, 0, canvas.width, canvas.height);

    ctx.drawImage(canvas, 0, 0);
    ctx.drawImage(drawingCanvas, 0, 0);

    const link = $.output;
    link.setAttribute('download', `${STATE.general.fileName}.png`);
    const image = outputCanvas
      .toDataURL('image/png')
      .replace('image/png', 'image/octet-stream');
    link.setAttribute('href', image);
    link.click();
  }

  function handleAddFile(side: Side): () => void {
    return () => {
      const files = $[side].file.files;
      if (!files) {
        return;
      } else {
        switch (files.length) {
          case 0:
            throw new Error('0 files added');
          case 1:
            const file = files.item(0);
            STATE[side].file = file;
            drawSide(side);
            return;
          default:
            throw new Error('More than one file added');
        }
      }
    };
  }

  // *****
  // UTILS
  // *****
  //
  // This section contains general utilities.

  function setup() {
    const rect = $.content.canvasWrapper.getBoundingClientRect();
    $.content.canvas.width = rect.width * window.devicePixelRatio;
    $.content.canvas.height = rect.height * window.devicePixelRatio;

    const ctx = getContext($.content.canvas);

    ctx.scale(window.devicePixelRatio, window.devicePixelRatio);
  }

  function draw() {
    setup();
    drawSide(Side.before);
    drawSide(Side.after);
  }

  function drawSide(side: Side) {
    clearCanvasSide(side);
    try {
      drawFile(side).then(() => drawText(side));
    } catch {
      drawText(side);
    }
  }

  async function drawFile(side: Side): Promise<void> {
    const ctx = getContext($.content.canvas);

    const {file} = STATE[side];
    if (file === null) throw new Error('cannot draw without a file');

    const width = ctx.canvas.width / window.devicePixelRatio;
    const halfCanvas = width / 2;

    const image = await getImage(side);
    return ctx.drawImage(
      image,
      0,
      0,
      image.width,
      image.height,
      side === Side.before ? 0 : halfCanvas,
      0,
      halfCanvas,
      getImageResizeHeight(image, halfCanvas),
    );
  }

  function drawText(side: Side) {
    const ctx = $.content.canvas.getContext('2d');
    if (!ctx) return;

    const width = ctx.canvas.width / window.devicePixelRatio;
    const {text} = STATE[side];

    if (!text) return;

    ctx.font = '20px Sans-serif';
    ctx.fillStyle = 'white';
    ctx.lineWidth = 2;
    ctx.strokeText(text, 10 + (side === Side.before ? 0 : width / 2), 30);
    ctx.fillText(text, 10 + (side === Side.before ? 0 : width / 2), 30);
  }

  function clearCanvasSide(side: Side): void {
    const ctx = getContext($.content.canvas);
    if (!ctx) return;
    // Store the current transformation matrix
    ctx.save();

    // Use the identity matrix while clearing the canvas
    ctx.setTransform(1, 0, 0, 1, 0, 0);

    const {width, height} = ctx.canvas;
    const topLeft = {
      x: side === Side.before ? 0 : width / 2,
      y: 0,
    };
    ctx.clearRect(topLeft.x, topLeft.y, width / 2, height);

    // Restore the transform
    ctx.restore();
  }

  function fileToImageSrc(file: File): Promise<string> {
    const reader = new FileReader();
    return new Promise(resolve => {
      reader.onloadend = () => {
        const {result} = reader;
        if (!result) throw new Error('could not convert file to image src');
        resolve(result as string);
      };
      reader.readAsDataURL(file);
    });
  }

  function getImage(side: Side): Promise<HTMLImageElement> {
    const {file} = STATE[side];
    if (!file) throw new Error('file is not defined!');
    return new Promise(resolve =>
      fileToImageSrc(file).then(imageSrc => {
        const image = new Image();
        image.onload = () => resolve(image);
        image.src = imageSrc;
      }),
    );
  }

  function getImageResizeHeight(
    image: HTMLImageElement,
    newWidth: number,
  ): number {
    const {width, height} = image;
    const aspectRatio = width / height;
    return newWidth / aspectRatio;
  }

  function validateForm(handler: Function) {
    return (...args: any[]) => {
      handler(...args);
      if (STATE.general.fileName && STATE.before.file && STATE.after.file)
        $.submit.disabled = false;
      else $.submit.disabled = true;
    };
  }

  function getContext(canvas: HTMLCanvasElement): CanvasRenderingContext2D {
    const ctx = canvas.getContext('2d');
    if (!ctx) throw new Error('cannot find context!');
    return ctx;
  }
})();
